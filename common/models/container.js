'use strict';

module.exports = function(Container) {
    Container.afterRemote('upload', function(ctx, data, next) {
        const newName = ctx.req.query.name;
        const {container, name} = data.result.files.file[0];
    
        // file wants to be renamed
        if (newName) {
    
          // create new name with old ext
          let newFullName = `${newName}`;
    
          // pipe old file as new one with new name
          let dlStream = Container.downloadStream(container, name);
          let ulStream = Container.uploadStream(container, newFullName);
          dlStream.pipe(ulStream);
          ulStream.on('finish', () => {
            Container.removeFile(container, name, (err) => {
              next();
            });
          });
        } else next();
      });
};
