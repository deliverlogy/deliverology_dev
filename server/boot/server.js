'use strict';

module.exports = function (app) {
    var nodemailer = require('nodemailer');
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'deliverologytest@gmail.com',
            pass: 'deliverology'
        }
    });
    var User = app.models.User;
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;
    var Milestone = app.models.Milestone;
    var Strategy = app.models.Strategy;
    var Activity = app.models.Activity;
    var University = app.models.University;
    var Activitystatusupdate = app.models.ActivityStatusUpdate;
    var monthsValue = [{
            'name': 'jan',
            'date': 31
        },
        {
            'name': 'feb',
            'date': 28
        },
        {
            'name': 'mar',
            'date': 31
        },
        {
            'name': 'apr',
            'date': 30
        },
        {
            'name': 'may',
            'date': 31
        },
        {
            'name': 'jun',
            'date': 30
        },
        {
            'name': 'jul',
            'date': 31
        },
        {
            'name': 'aug',
            'date': 31
        },
        {
            'name': 'sep',
            'date': 30
        },
        {
            'name': 'oct',
            'date': 31
        },
        {
            'name': 'nov',
            'date': 30
        },
        {
            'name': 'dec',
            'date': 31
        }
    ];


    // Create User

    // User.create([{
    //     username: 'admin',
    //     email: 'admin@deliverology.com',
    //     password: 'admin',
    //     firstName: 'admin',
    //     lastName: 'admin',
    //     phone: '0',
    //     organization: 'admin',
    //     active: true
    // }, {
    //     username: 'admin1',
    //     email: 'admin1@deliverology.com',
    //     password: 'admin',
    //     firstName: 'admin',
    //     lastName: 'admin',
    //     phone: '0',
    //     organization: 'admin',
    //     active: true
    // }, {
    //     username: 'admin2',
    //     email: 'admin2@deliverology.com',
    //     password: 'admin',
    //     firstName: 'admin',
    //     lastName: 'admin',
    //     phone: '0',
    //     organization: 'admin',
    //     active: true
    // }], function (err, users) {
    //     if (err) throw err;

    //     console.log('Created users:', users)
    //     //create the admin role
    //     Role.create({
    //         name: 'admin'
    //     }, function (err, role) {
    //         if (err) throw err;

    //         console.log('Created role:', role);

    //         for (let user = 0; user < users.length; user++) {
    //             role.principals.create({
    //                 principalType: RoleMapping.USER,
    //                 principalId: users[user].id
    //             }, function (err, principal) {
    //                 if (err) throw err;

    //                 console.log('Created principal:', principal);
    //             });
    //         }
    //     });
    // });

    // End Create User

    University.getUniversityStrategiesWithStatus = function (universityId, weekNumber, year, cb) {
        var organization = '';
        if (universityId == 'moe') {
            organization = 'moe';
        } else {
            organization = 'university';
        }
        var strategies = [];
        Strategy.find({
            where: {
                'visible': true,
                'valid': true
            }
        }, function (err, strRes) {
            strategies = strRes;
            // cb(null, strategies);
            for (let si = 0; si < strategies.length; si++) {
                strategies[si].startMonth = 0;
                strategies[si].endMonth = 0;
                strategies[si].endYear = 0;
                strategies[si].startYear = 0;
                strategies[si].milestone = [];

                Milestone.find({
                    where: {
                        'strategyId': strategies[si].id,
                        'visible': true,
                        'valid': true
                    }
                }, function (err, mileRes) {
                    strategies[si].milestone = mileRes;
                    // cb(null, strategies);
                    for (let mi = 0; mi < strategies[si].milestone.length; mi++) {
                        strategies[si].milestone[mi].startMonth = strategies[si].milestone[mi].startDate.substring(5, 7);
                        strategies[si].milestone[mi].startDay = strategies[si].milestone[mi].startDate.substring(8, 10);
                        strategies[si].milestone[mi].endMonth = strategies[si].milestone[mi].endDate.substring(5, 7);
                        strategies[si].milestone[mi].endDay = strategies[si].milestone[mi].endDate.substring(8, 10);
                        strategies[si].milestone[mi].startYear = strategies[si].milestone[mi].startDate.substring(0, 4);
                        strategies[si].milestone[mi].endYear = strategies[si].milestone[mi].endDate.substring(0, 4);


                        strategies[si].startYear = strategies[si].milestone[mi].startDate.substring(0, 4);
                        strategies[si].endYear = strategies[si].milestone[mi].endDate.substring(0, 4);

                        if ((strategies[si].startMonth > strategies[si].milestone[mi].startMonth &&
                                strategies[si].startYear >= strategies[si].milestone[mi].startYear) || strategies[si].startMonth == 0) {
                            strategies[si].startMonth = strategies[si].milestone[mi].startMonth;
                        }
                        if ((strategies[si].endMonth < strategies[si].milestone[mi].endMonth &&
                                strategies[si].endYear <= strategies[si].milestone[mi].endYear) || strategies[si].endMonth == 0) {
                            strategies[si].endMonth = strategies[si].milestone[mi].endMonth;
                        }

                        strategies[si].milestone[mi].months = [];

                        if (strategies[si].endYear == strategies[si].startYear) {
                            strategies[si].length = strategies[si].endMonth - strategies[si].startMonth;
                            for (let k = 0; k <= strategies[si].length; k++) {
                                strategies[si].milestone[mi].months.push(parseInt((strategies[si].startMonth), 0) + k);
                            }

                        } else if (strategies[si].endYear > strategies[si].startYear) {
                            var a = 0;
                            var l = 0;
                            var tempLength
                            var length = 0;
                            for (let aoi = 0; aoi <= a; aoi++) {
                                if (aoi == 0) {
                                    tempLength = 12 - strategies[si].startMonth;
                                    length += tempLength;
                                    for (let k = 0; k <= tempLength; k++) {
                                        strategies[si].milestone[mi].months.push(parseInt((strategies[si].startMonth), 0) + k);
                                    }
                                } else if (strategies[si].startYear < (strategies[si].startYear + aoi) > strategies[si].endYear) {
                                    tempLength = 12;
                                    length += tempLength;
                                    for (let k = 0; k <= tempLength; k++) {
                                        strategies[si].milestone[mi].months.push(1 + k);
                                    }
                                } else if (aoi == a) {
                                    tempLength = strategies[si].endMonth;
                                    length += tempLength;
                                    for (let k = 0; k <= tempLength; k++) {
                                        strategies[si].milestone[mi].months.push(1 + k);
                                    }
                                }
                            }

                            strategies[si].length = length;
                        }
                        strategies[si].milestone[mi].activity = [];
                        Activity.find({
                            where: {
                                'milestoneId': strategies[si].milestone[mi].id,
                                'visible': true
                            }
                        }, function (err, actRes) {
                            strategies[si].milestone[mi].activity = actRes;
                            for (let ai = 0; ai < strategies[si].milestone[mi].activity.length; ai++) {
                                strategies[si].milestone[mi].activity[ai].startMonth = parseInt(strategies[si].milestone[mi].activity[ai].startDate.substring(5, 7), 0);
                                strategies[si].milestone[mi].activity[ai].endMonth = parseInt(strategies[si].milestone[mi].activity[ai].endDate.substring(5, 7), 0);
                                strategies[si].milestone[mi].activity[ai].startDay = parseInt(strategies[si].milestone[mi].activity[ai].startDate.substring(8, 10), 0);
                                strategies[si].milestone[mi].activity[ai].endDay = parseInt(strategies[si].milestone[mi].activity[ai].endDate.substring(8, 10), 0);
                                strategies[si].milestone[mi].activity[ai].startYear = parseInt(strategies[si].milestone[mi].activity[ai].startDate.substring(0, 4), 0);
                                strategies[si].milestone[mi].activity[ai].endYear = parseInt(strategies[si].milestone[mi].activity[ai].endDate.substring(0, 4), 0);
                                strategies[si].milestone[mi].activity[ai].months = [];
                                strategies[si].milestone[mi].activity[ai].fileToUpload = null;

                                // var year = parseInt(strategies[si].milestone[mi].startDate.substring(0, 4), 0);
                                for (let j = 0; j < strategies[si].milestone[mi].months.length; j++) {
                                    let dates = [];
                                    var weeks = 5;
                                    if (strategies[si].milestone[mi].months[j] == 2) {
                                        weeks = 4;
                                    }
                                    for (let k = 1; k <= weeks; k++) {
                                        if (parseInt(strategies[si].milestone[mi].activity[ai].startYear, 0) === parseInt(strategies[si].milestone[mi].activity[ai].endYear, 0)) {
                                            if (strategies[si].milestone[mi].activity[ai].startMonth > parseInt(strategies[si].milestone[mi].months[j], 0) ||
                                                strategies[si].milestone[mi].activity[ai].endMonth < parseInt(strategies[si].milestone[mi].months[j], 0)) {
                                                dates.push(0);
                                            } else if (strategies[si].milestone[mi].activity[ai].startMonth == strategies[si].milestone[mi].activity[ai].endMonth) {
                                                if (strategies[si].milestone[mi].activity[ai].startDay <= k * 7 && strategies[si].milestone[mi].activity[ai].endDay >= k * 7) {
                                                    dates.push(1);
                                                } else {
                                                    dates.push(0);
                                                }
                                            } else if (strategies[si].milestone[mi].activity[ai].startMonth == parseInt(strategies[si].milestone[mi].months[j], 0)) {
                                                if (strategies[si].milestone[mi].activity[ai].startDay <= k * 7) {
                                                    dates.push(1);
                                                } else {
                                                    dates.push(0);
                                                }
                                            } else if (strategies[si].milestone[mi].activity[ai].endMonth == parseInt(strategies[si].milestone[mi].months[j], 0)) {
                                                if (strategies[si].milestone[mi].activity[ai].endDay >= k * 7) {
                                                    dates.push(1);
                                                } else {
                                                    dates.push(0);
                                                }
                                            } else {
                                                dates.push(1);
                                            }
                                        } else {
                                            dates.push(0);
                                        }
                                    }
                                    strategies[si].milestone[mi].activity[ai].months.push(dates);
                                    dates = [];
                                }

                                // strategies[si].milestone[mi].activity[ai].statusUpdate = [];

                                if (weekNumber == 0 && year == 0) {
                                    Activitystatusupdate.find({
                                        where: {
                                            'activityId': strategies[si].milestone[mi].activity[ai].id,
                                            'universityId': universityId,
                                            'visible': true
                                        }
                                    }, function (err, statusRes) {
                                        if (statusRes.length != 0) {
                                            if (statusRes[statusRes.length - 1].status == "yellow" || statusRes[statusRes.length - 1].status == "#aae414") {
                                                statusRes[statusRes.length - 1].status = "#aae414";
                                            } else if (statusRes[statusRes.length - 1].status == "orange" || statusRes[statusRes.length - 1].status == "#e38634") {
                                                statusRes[statusRes.length - 1].status = "#e38634";
                                            }
                                            strategies[si].milestone[mi].activity[ai].statusUpdate = statusRes[statusRes.length - 1];
                                        } else {
                                            strategies[si].milestone[mi].activity[ai].statusUpdate = {
                                                id: '',
                                                status: 'white',
                                                weekOfStatus: 0,
                                                weekOfStatusYear: 0,
                                                attachmentId: '',
                                                comment: '',
                                                submittedBy: '',
                                                verified: true
                                            };
                                        }
                                        if ((((strategies.length == 0) ? 0 : si + 1) == strategies.length) &&
                                            (((strategies[si].milestone.length == 0) ? 0 : mi + 1) == strategies[si].milestone.length) &&
                                            (((strategies[si].milestone[mi].activity.length == 0) ? 0 : ai + 1) == strategies[si].milestone[mi].activity.length)) {
                                            setTimeout(() => {
                                                cb(null, strategies);
                                            }, 1000);
                                        }
                                    });
                                } else {
                                    Activitystatusupdate.find({
                                        where: {
                                            'activityId': strategies[si].milestone[mi].activity[ai].id,
                                            'weekOfStatus': weekNumber,
                                            'weekOfStatusYear': year,
                                            'universityId': universityId,
                                            'visible': true
                                        }
                                    }, function (err, statusRes) {
                                        if (statusRes.length != 0) {
                                            if (statusRes[statusRes.length - 1].status == "yellow" || statusRes[statusRes.length - 1].status == "#aae414") {
                                                statusRes[statusRes.length - 1].status = "#aae414";
                                            } else if (statusRes[statusRes.length - 1].status == "orange" || statusRes[statusRes.length - 1].status == "#e38634") {
                                                statusRes[statusRes.length - 1].status = "#e38634";
                                            }
                                            strategies[si].milestone[mi].activity[ai].statusUpdate = statusRes[0];
                                        } else {
                                            strategies[si].milestone[mi].activity[ai].statusUpdate = {
                                                id: '',
                                                status: 'white',
                                                weekOfStatus: 0,
                                                weekOfStatusYear: 0,
                                                attachmentId: '',
                                                comment: '',
                                                submittedBy: '',
                                                verified: true
                                            };
                                        }
                                        if ((((strategies.length == 0) ? 0 : si + 1) == strategies.length) &&
                                            (((strategies[si].milestone.length == 0) ? 0 : mi + 1) == strategies[si].milestone.length) &&
                                            (((strategies[si].milestone[mi].activity.length == 0) ? 0 : ai + 1) == strategies[si].milestone[mi].activity.length)) {
                                            setTimeout(() => {
                                                cb(null, strategies);
                                            }, 1000);
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }

        });
    }

    University.remoteMethod(
        'getUniversityStrategiesWithStatus', {
            http: {
                path: '/getUniversityStrategiesWithStatus',
                verb: 'get'
            },
            accepts: [{
                arg: 'universityId',
                type: 'string',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'weekNumber',
                type: 'number',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'year',
                type: 'number',
                http: {
                    source: 'query'
                }
            }],
            returns: {
                arg: 'strategies',
                type: 'object'
            }
        }
    );

    Activitystatusupdate.getMoeStatusUpdate = function (weekNumber, year, cb) {
        var status = {};
        status.green = 0;
        status.yellow = 0;
        status.orange = 0;
        status.red = 0;
        var strategies = [];
        Strategy.find({
            where: {
                'visible': true,
                'valid': true
            }
        }, function (err, strRes) {
            strategies = strRes;
            for (let si = 0; si < strategies.length; si++) {
                Milestone.find({
                    where: {
                        'strategyId': strategies[si].id,
                        'visible': true,
                        'valid': true
                    }
                }, function (err, mileRes) {
                    strategies[si].milestone = mileRes;
                    strategies[si].green = 0;
                    strategies[si].amberGreen = 0;
                    strategies[si].amberRed = 0;
                    strategies[si].red = 0;
                    strategies[si].counter = 0;

                    for (let mi = 0; mi < strategies[si].milestone.length; mi++) {
                        strategies[si].milestone[mi].activity = [];
                        Activity.find({
                            where: {
                                'milestoneId': strategies[si].milestone[mi].id,
                                'visible': true
                            }
                        }, function (err, actRes) {
                            strategies[si].milestone[mi].activity = actRes;

                            for (let ai = 0; ai < strategies[si].milestone[mi].activity.length; ai++) {
                                Activitystatusupdate.find({
                                    where: {
                                        'activityId': strategies[si].milestone[mi].activity[ai].id,
                                        'universityId': 'moe',
                                        'weekOfStatus': weekNumber,
                                        'weekOfStatusYear': year,
                                        'visible': true
                                    }
                                }, function (err, statusRes) {
                                    // strategies[si].milestone[mi].activity[ai].statusUpdate = statusRes[statusRes.length - 1];
                                    if (statusRes.length != 0) {
                                        if (statusRes[0].status == 'green') {
                                            strategies[si].green += 1;
                                            status.green += 1;
                                        } else if (statusRes[0].status == '#aae414' || statusRes[0].status == 'yellow') {
                                            strategies[si].amberGreen += 1;
                                            status.yellow += 1;
                                        } else if (statusRes[0].status == '#e38634' || statusRes[0].status == 'orange') {
                                            strategies[si].amberRed += 1;
                                            status.orange += 1;
                                        } else if (statusRes[0].status == 'red') {
                                            strategies[si].red += 1;
                                            status.red += 1;
                                        }
                                        strategies[si].counter += 1;
                                    }

                                    if (
                                        ((strategies.length == 0) ? 0 : si + 1 == strategies.length) &&
                                        ((strategies[si].milestone.length == 0) ? 0 : mi + 1 == strategies[si].milestone.length) &&
                                        ((strategies[si].milestone[mi].activity.length == 0) ? 0 : ai + 1 == strategies[si].milestone[mi].activity.length)) {
                                        setTimeout(() => {
                                            cb(null, status);
                                        }, 1000);

                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }


    Activitystatusupdate.remoteMethod(
        'getMoeStatusUpdate', {
            http: {
                path: '/getMoeStatusUpdate',
                verb: 'get'
            },
            accepts: [{
                arg: 'weekNumber',
                type: 'number',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'year',
                type: 'number',
                http: {
                    source: 'query'
                }
            }],
            returns: {
                arg: 'status',
                type: 'object'
            }
        }
    );

    University.getUniversitiesStatusUpdate = function (weekNumber, year, cb) {
        var universities = [];
        University.find({
            where: {
                'visible': true
            },
            fields: ['id', 'universityName']
        }, function (err, uniRes) {
            universities = uniRes;
            var count = 0;
            for (let ui = 0; ui < universities.length; ui++) {
                Strategy.find({
                    where: {
                        'visible': true,
                        'valid': true
                    },
                    fields: ['id', 'strategyName'],
                    include: {
                        relation: 'milestones',
                        scope: {
                            fields: ['id'],
                            where: {
                                'visible': true,
                                'valid': true
                            },
                            include: {
                                relation: 'activities',
                                scope: {
                                    fields: ['id'],
                                    where: {
                                        'visible': true,
                                        'owner': 'university'
                                    },
                                    include: {
                                        relation: 'activityStatusUpdates',
                                        scope: {
                                            where: {
                                                'visible': true,
                                                'universityId': universities[ui].id,
                                                'weekOfStatus': weekNumber,
                                                'weekOfStatusYear': year
                                            },
                                            fields: ['status']
                                        }
                                    }
                                }
                            }
                        }
                    }

                }, function (err, strRes) {
                    universities[ui].strategies = strRes;
                    count += 1;
                    if (universities.length === count) {
                        cb(null, universities);
                    }
                });

            }
        });
    }

    University.remoteMethod(
        'getUniversitiesStatusUpdate', {
            http: {
                path: '/getUniversitiesStatusUpdate',
                verb: 'get'
            },
            accepts: [{
                arg: 'weekNumber',
                type: 'number',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'year',
                type: 'number',
                http: {
                    source: 'query'
                }
            }],
            returns: {
                arg: 'universities',
                type: 'array'
            }
        }
    );

    Strategy.getStrategies = function (yearId, cb) {
        var strategies = [];
        Strategy.find({
            where: {
                'visible': true
            },
            include: {
                relation: 'milestones',
                scope: {
                    where: {
                        'visible': true,
                        'yearId': yearId
                    },
                    include: {
                        relation: 'activities',
                        scope: {
                            where: {
                                'visible': true,
                                'yearId': yearId
                            }
                        }
                    }
                }
            }
        }, function (err, strRes) {
            strategies = strRes;
            cb(null, strategies);
        });
    }

    Strategy.remoteMethod(
        'getStrategies', {
            http: {
                path: '/getStrategies',
                verb: 'get'
            },
            accepts: [{
                arg: 'yearId',
                type: 'string',
                http: {
                    source: 'query'
                }
            }],
            returns: {
                arg: 'strategies',
                type: 'object'
            }
        }
    );

    Activitystatusupdate.getUniversitiesCount = function (weekNumber, cb) {
        var counts = [{}, {}, {}, {}, {}, {}];

        for (let i = 0; i <= 5; i++) {
            setTimeout(() => {
                Activitystatusupdate.find({
                    where: {
                        'visible': true,
                        'weekOfStatus': weekNumber - i
                    }
                }, function (err, res) {
                    setTimeout(() => {
                        var universities = [];
                        setTimeout(() => {
                            for (let j = 0; j < res.length; j++) {
                                if (universities.indexOf(res[j].universityId + '') === -1 && res[j].universityId !== 'moe') {
                                    universities.push(res[j].universityId + '');
                                }
                            }
                        }, 0);
                        if (res.length !== 0) {
                            counts[i].date = res[0].submitionDate;
                        } else {
                            counts[i].date = 0;
                        }
                        counts[i].universities = universities;
                        setTimeout(() => {
                            if (i === 5) {
                                cb(null, counts);
                            }
                        }, 0);
                    }, 100);

                });
            }, 0);

        }
    }

    Activitystatusupdate.remoteMethod(
        'getUniversitiesCount', {
            http: {
                path: '/getUniversitiesCount',
                verb: 'get'
            },
            accepts: {
                arg: 'weekNumber',
                type: 'number',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'count',
                type: 'object'
            }
        }
    );

    Activitystatusupdate.decline = function (activity, id, owner, date, userId, cb) {
        User.find({
            where: {
                'id': userId
            }
        }, function (err, res) {
            const mailOptions = {
                from: 'deliverologytest@gmail.com', // sender address
                to: res[0].email, // list of receivers
                subject: 'Please resend a valid and proper evidence.', // Subject line
                html: '<p>Dear ' + res[0].firstName + ' ' + res[0].lastName + ', DFP of ' + owner + '</p>' +
                    '<p>The evidence you attached to the activity ' + activity + ' on ' + date +
                    ' is reviewed and found not satisfactory. Please send a valid and proper evidence through email explaining the activity accomplished.</p></hr> <p><strong>MDU - HE</strong></p>' // html body
            };
            transporter.sendMail(mailOptions, function (err, info) {
                if (err)
                    console.log(err)
                else
                    console.log(info);
            });

            Activitystatusupdate.update({
                'id': id
            }, {
                'visible': false,
                'verified': true
            }, function (err, statusRes) {
                if (err)
                    console.log(err);
                cb(null, statusRes);
            });
        });
    }

    Activitystatusupdate.remoteMethod(
        'decline', {
            http: {
                path: '/decline',
                verb: 'get'
            },
            accepts: [{
                arg: 'activity',
                type: 'string',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'id',
                type: 'string',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'date',
                type: 'string',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'owner',
                type: 'string',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'userId',
                type: 'string',
                http: {
                    source: 'query'
                }
            }],
            returns: {
                arg: 'res',
                type: 'array'
            }
        }
    );

    Strategy.deleteStrategy = function (strategyId, cb) {
        Strategy.update({
            'id': strategyId
        }, {
            'visible': false
        }, function (err, strRes) {
            Milestone.find({
                where: {
                    'strategyId': strategyId
                }
            }, function (err, mileRes) {
                if (mileRes.length != 0) {
                    for (let mi = 0; mi < mileRes.length; mi++) {
                        Milestone.update({
                            'id': mileRes[mi].id
                        }, {
                            'visible': false
                        }, function (err, mileUpdateRes) {
                            Activity.find({
                                where: {
                                    'milestoneId': mileRes[mi].id
                                }
                            }, function (err, actRes) {
                                if (actRes.length != 0) {
                                    for (let ai = 0; ai < actRes.length; ai++) {
                                        Activity.update({
                                            'id': actRes[ai].id
                                        }, {
                                            'visible': false
                                        }, function (err, actUpdateRes) {});
                                    }
                                }
                            });
                        });

                    }
                }
            });
            cb(null, []);
        });
    }

    Strategy.remoteMethod(
        'deleteStrategy', {
            http: {
                path: '/deleteStrategy',
                verb: 'get'
            },
            accepts: {
                arg: 'strategyId',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'strategy',
                type: 'array'
            }
        }
    );

    Milestone.deleteMilestone = function (milestoneId, cb) {
        Milestone.findOne({
            where: {
                'id': milestoneId
            }
        }, function (err, mileRes) {
            Milestone.find({
                where: {
                    'strategyId': mileRes.strategyId,
                    'visible': true
                }
            }, function (err, milestones) {
                if (milestones.length == 1) {
                    Strategy.update({
                        'id': mileRes.strategyId
                    }, {
                        'valid': false
                    }, function (err, strRes) {});
                }
                Milestone.update({
                    'id': milestoneId
                }, {
                    'visible': false
                }, function (err, mileUpdateRes) {
                    Activity.find({
                        where: {
                            'milestoneId': milestoneId
                        }
                    }, function (err, actRes) {
                        if (actRes.length != 0) {
                            for (let ai = 0; ai < actRes.length; ai++) {
                                Activity.update({
                                    'id': actRes[ai].id
                                }, {
                                    'visible': false
                                }, function (err, actUpdateRes) {});
                            }
                        }
                    });
                });
            });
            cb(null, []);
        });
    }

    Milestone.remoteMethod(
        'deleteMilestone', {
            http: {
                path: '/deleteMilestone',
                verb: 'get'
            },
            accepts: {
                arg: 'milestoneId',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'milestone',
                type: 'array'
            }
        }
    );


    Activity.deleteActivity = function (activityId, cb) {
        Activity.findOne({
            where: {
                'id': activityId
            }
        }, function (err, actRes) {
            Activity.find({
                where: {
                    'milestoneId': actRes.milestoneId,
                    'visible': true
                }
            }, function (err, milestones) {
                if (milestones.length == 1) {
                    Milestone.update({
                        'id': actRes.milestoneId
                    }, {
                        'valid': false
                    }, function (err, mileUpdateRes) {
                        Milestone.findOne({
                            where: {
                                'id': actRes.milestoneId
                            }
                        }, function (err, mileRes) {
                            Milestone.find({
                                where: {
                                    'strategyId': mileRes.strategyId,
                                    'visible': true
                                }
                            }, function (err, milestones) {
                                if (milestones.length == 1) {
                                    Strategy.update({
                                        'id': mileRes.strategyId
                                    }, {
                                        'valid': false
                                    }, function (err, strRes) {});
                                }
                            });
                        });
                    });
                }
                Activity.update({
                    'id': activityId
                }, {
                    'visible': false
                }, function (err, actUpdateRes) {});
            });
            cb(null, []);
        });
    }

    Activity.remoteMethod(
        'deleteActivity', {
            http: {
                path: '/deleteActivity',
                verb: 'get'
            },
            accepts: {
                arg: 'activityId',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'activity',
                type: 'array'
            }
        }
    );

    Activity.addActivity = function (data, cb) {
        Activity.create(data, function (err, actCreateRes) {
            Milestone.findOne({
                where: {
                    'id': data.milestoneId
                }
            }, function (err, mileRes) {
                Milestone.update({
                    'id': data.milestoneId
                }, {
                    'valid': true
                }, function (err, mileUpdateRes) {
                    Strategy.update({
                        'id': mileRes.strategyId
                    }, {
                        'valid': true
                    }, function (err, strUpdateRes) {
                        cb(null, []);
                    });
                });
            });
        });
    }

    Activity.remoteMethod(
        'addActivity', {
            http: {
                path: '/addActivity',
                verb: 'post'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                'http': {
                    source: 'body'
                }
            },
            returns: {
                arg: 'activity',
                type: 'array'
            }
        }
    );
}
